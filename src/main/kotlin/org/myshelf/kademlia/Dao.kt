package org.myshelf.kademlia

import javafx.collections.transformation.SortedList
import java.math.BigInteger
import java.net.InetAddress
import java.nio.ByteBuffer
import java.util.*
import java.util.concurrent.ConcurrentLinkedDeque
import java.util.concurrent.ConcurrentSkipListMap
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.experimental.xor

const val KEY_LENGTH_BIT = 160
const val KEY_LENGTH_BYTES = KEY_LENGTH_BIT / 8
const val REPLICATION_PARAM = 20

//TODO: Async

data class Node(
        val nodeID: ByteArray = generateNodeID(),
        val address: String,
        val port: Int,
        private val buckets: List<KBucket> = CopyOnWriteArrayList<KBucket>(Array(KEY_LENGTH_BIT, { KBucket() }))
) {
}

class KBucket(
        private val nodes: Deque<Node> = ConcurrentLinkedDeque<Node>()
) {
    /**
     * Adds a node to the buckets or moves it to the bottom of the bucket.
     */
    fun put(node: Node) {
        if (this.nodes.contains(node)) {
            // Move node to the bottom as it appeared at last
            this.nodes.remove(node)
            this.nodes.addLast(node)
        } else {
            // Doesn't contain the node yet so add it
            this.nodes.addLast(node)
            if (this.nodes.size > REPLICATION_PARAM) {
                // TODO: Ping least recently seen;
                // If respond: move that to the bottom and discard the new node
                // If no respond: remove old from top and add new to tail
            }
        }
    }
}

data class Message(
        val senderId: ByteArray,
        val data: String
)

// Kademlia algorithms and functions

class Network() {
    // Add an entry to the network
    fun store(node: Node, key: ByteArray, value: ByteArray): Boolean {
        // Keys have to be 160 Bit = 20 Byte long
        if (key.size != KEY_LENGTH_BYTES) {
            return false
        }

        return false
    }

    // Check if the node is available
    fun ping(node: Node) {

    }

    fun findNode(id: ByteArray) {

    }

    fun findValue(node: Node) {

    }
}

// Distance between nodes
fun Node.abs(node: Node): BigInteger = this.nodeID xor node.nodeID

// XOR between ByteArrays
private infix fun ByteArray.xor(nodeID: ByteArray): BigInteger {
    val xor = ByteArray(KEY_LENGTH_BYTES)

    for (i in 1..KEY_LENGTH_BYTES) {
        xor[i] = this[i] xor nodeID[i]
    }

    // Shouldn't be a problem as 20 Byte
    return BigInteger(xor)
}

// Generate a new NodeID
fun generateNodeID(): ByteArray {
    TODO()
}